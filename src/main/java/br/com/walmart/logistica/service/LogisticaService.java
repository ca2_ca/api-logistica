package br.com.walmart.logistica.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.walmart.logistica.model.Mapa;
import br.com.walmart.logistica.model.Rota;
import br.com.walmart.logistica.enums.TipoRelacionamento;
import br.com.walmart.logistica.model.RotaCompleta;
import com.google.gson.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.neo4j.graphdb.Relationship;
import org.neo4j.rest.graphdb.RestAPI;
import org.neo4j.rest.graphdb.RestAPIFacade;
import org.neo4j.rest.graphdb.entity.RestNode;
import org.neo4j.rest.graphdb.query.QueryEngine;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

import br.com.walmart.logistica.model.CaminhoDistancia;

public class LogisticaService {
	private static String NOME = "nome";
	private static String DISTANCIA = "distancia";
	
	private RestAPI api;
	
	public LogisticaService() {
		api = new RestAPIFacade("http://127.0.0.1:7474/db/data/");
	}

	public void adicionar(Mapa mapa) {
		for (String nodeName : createDistinctNodes(mapa)) {
			criarNo(nodeName);
		}

		for (Rota rota : mapa.getRotas()) {
			criarRelacionamento(rota.getOrigem(), rota.getDestino(), rota.getDistancia());
		}
	}
	
	public RotaCompleta obterMelhorRota(String origem, String destino, double autonomia, double valorLitro) throws IOException {

		RotaCompleta rotaCompleta = null;
		RestNode noOrigem = buscarNo(origem);
		RestNode noDestino = buscarNo(destino);
		
		if (noOrigem == null) {
			throwException(404, "Nao foi encontrado o no de origem!");
		}
		
		if (noDestino == null) {
			throwException(404, "Nao foi encontrado o no de destino");
		}

		CaminhoDistancia caminhoDistancia = buscarUnicoCaminho(noOrigem, noDestino);
		rotaCompleta = createRouteDirections(caminhoDistancia, autonomia, valorLitro);
		
		return rotaCompleta;
	}
	
	private Set<String> createDistinctNodes(Mapa mapa) {
		Set<String> distinctNodes = new HashSet<String>();
		
		for (Rota rota : mapa.getRotas()) {
			distinctNodes.add(rota.getOrigem());
			distinctNodes.add(rota.getDestino());
		}
		
		return distinctNodes;
	}
	
	private RestNode criarNo(String nome) {
		java.util.Map<String, Object> props = new HashMap<String, Object>();
		props.put(NOME, nome);
		
		return api.createNode(props);
	}
	
	private void criarRelacionamento(String origem, String destino, double distancia) {
		RestNode noOrigem = buscaCriaNo(origem);
		RestNode noDestino = buscaCriaNo(destino);
		
		java.util.Map<String, Object> props = new HashMap<String, Object>();
		props.put(DISTANCIA, distancia);
		
		Relationship r = noOrigem.createRelationshipTo(noDestino, TipoRelacionamento.LEVA_ATE);
		r.setProperty(DISTANCIA, distancia);
	}
	
	private RestNode buscaCriaNo(String nome) {
		RestNode no = buscarNo(nome);
        
        if (no == null) {
            no = criarNo(nome);
        }
        
        return no;
	}
	
	private RestNode buscarNo(String nome) {

		String query;
		RestNode noEncontrado = null;

		QueryEngine engine = new RestCypherQueryEngine(api);
		query = String.format("MATCH (n {nome: '%s'}) RETURN n", nome);
		QueryResult<java.util.Map<String,Object>> result = engine.query(query, Collections.EMPTY_MAP);
		
		Iterator<java.util.Map<String, Object>> it = result.iterator();
		if (it.hasNext()) {
			java.util.Map<String,Object> row = it.next();
			noEncontrado = (RestNode) row.get("n");
		}
		
		return noEncontrado;
	}
	
	private RotaCompleta createRouteDirections(CaminhoDistancia caminhoDistancia, double autonomia, double valorLitro) {
		double distancia = caminhoDistancia.getPeso();
		double custo = calcularCusto(autonomia, valorLitro, distancia);
		
		List<String> caminho = new ArrayList<String>();
		
		for (Long idNo : caminhoDistancia.getCaminho()) {
			RestNode no = api.getNodeById(idNo);
			caminho.add(no.getProperty(NOME).toString());
		}
		
		RotaCompleta rotaCompleta = new RotaCompleta(caminho, custo, distancia);
		
		return rotaCompleta;
	}
	
	private double calcularCusto(double autonomia, double valorLitro, double distancia) {
		return (valorLitro * distancia) / autonomia;
	}
	
	private void throwException(int statusCode, String message) {
		java.util.Map<String, Object> result = new HashMap<String, Object>();
		result.put("code", statusCode);
		result.put("message", message);
				
		Gson gson = new Gson();
	    String json = gson.toJson(result);
	    
		Response response = Response.status(statusCode)
				.entity(json)
				.build();
		
		throw new WebApplicationException(response);
	}

	public CaminhoDistancia buscarUnicoCaminho(RestNode noOrigem, RestNode noDestino) throws IOException {
		JsonObject jsonObject = doPost(noOrigem, noDestino);

		String PESO = "weight";
		JsonElement peso = jsonObject.get(PESO);
		double weight = peso.getAsDouble();

		String nos = "nodes";
		JsonElement nodes = jsonObject.get(nos);
		JsonArray jsonArray = nodes.getAsJsonArray();
		List<Long> caminho = new ArrayList<Long>();

        for (JsonElement jsonElement : jsonArray) {
            String nodeURI = jsonElement.getAsString();
            int idx = nodeURI.lastIndexOf('/');
            String idNo = nodeURI.substring(idx + 1);
            caminho.add(Long.parseLong(idNo));
        }

		CaminhoDistancia caminhoDistancia = new CaminhoDistancia(weight, caminho);

		return caminhoDistancia;
	}

	private JsonObject doPost(RestNode originNode, RestNode destinationNode) throws IOException {

		JsonObject retorno = null;

				DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(originNode.getUri() + "/path");

			String payload = createJsonPayload(destinationNode);

			StringEntity input = new StringEntity(payload);
			input.setContentType(MediaType.APPLICATION_JSON);
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();

			String line;
			while ((line = reader.readLine()) != null) {
				sb.append (line);
			}

			httpClient.getConnectionManager().shutdown();

			String jsonResponse = sb.toString();
			JsonParser parser = new JsonParser();

			retorno = (JsonObject) parser.parse(jsonResponse);


		return retorno;
	}

	private String createJsonPayload(RestNode destinationNode) {
		JsonObject relationships = new JsonObject();
		String TYPE = "type";
		relationships.addProperty(TYPE, TipoRelacionamento.LEVA_ATE.toString());
		String DIRECTION = "direction";
		String OUT = "out";
		relationships.addProperty(DIRECTION, OUT);

		JsonObject obj = new JsonObject();
		String TO = "to";
		obj.addProperty(TO, destinationNode.getUri());
		String COST_PROPERTY = "cost_property";
		String DISTANCIA = "distancia";
		obj.addProperty(COST_PROPERTY, DISTANCIA);
		String RELATIONSHIPS = "relationships";
		obj.add(RELATIONSHIPS, relationships);
		String ALGORITHM = "algorithm";
		String DIJKSTRA = "dijkstra";
		obj.addProperty(ALGORITHM, DIJKSTRA);

		return obj.toString();
	}
}

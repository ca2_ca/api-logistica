package br.com.walmart.logistica.model;

import java.util.List;

public class CaminhoDistancia {
	private double peso;
	private List<Long> caminho;
	
	public CaminhoDistancia() {
	}

	public CaminhoDistancia(double peso, List<Long> caminho) {
		this.peso = peso;
		this.caminho = caminho;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public List<Long> getCaminho() {
		return caminho;
	}

	public void setCaminho(List<Long> caminho) {
		this.caminho = caminho;
	}
}

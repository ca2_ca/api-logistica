package br.com.walmart.logistica.model;

import java.util.List;

public class Mapa {
	private String nome;
	private List<Rota> rotas;

	public Mapa() {
	}

	public Mapa(String nome, List<Rota> rotas) {
		this.nome = nome;
		this.rotas = rotas;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Rota> getRotas() {
		return rotas;
	}

	public void setRotas(List<Rota> rotas) {
		this.rotas = rotas;
	}

	@Override
	public String toString() {
		return String.format("Mapa [nome=%s, rotas=%s]", nome, rotas);
	}
}

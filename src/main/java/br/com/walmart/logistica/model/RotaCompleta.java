package br.com.walmart.logistica.model;

import java.util.List;

public class RotaCompleta {
	private List<String> caminho;
	private double custo;
	private double distancia;

	public RotaCompleta() {
	}

	public RotaCompleta(List<String> caminho, double custo, double distancia) {
		this.caminho = caminho;
		this.custo = custo;
		this.distancia = distancia;
	}

	public List<String> getCaminho() {
		return caminho;
	}

	public void setCaminho(List<String> caminho) {
		this.caminho = caminho;
	}

	public double getCusto() {
		return custo;
	}

	public void setCusto(double custo) {
		this.custo = custo;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
}

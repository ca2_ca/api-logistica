package br.com.walmart.logistica.enums;

import org.neo4j.graphdb.RelationshipType;

public enum TipoRelacionamento implements RelationshipType {
	LEVA_ATE
}
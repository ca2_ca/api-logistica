package br.com.walmart.logistica.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.walmart.logistica.service.LogisticaService;
import com.google.gson.Gson;
import br.com.walmart.logistica.model.Mapa;
import br.com.walmart.logistica.model.RotaCompleta;

@Path("/rota")
public class LogisticaController {
	@GET
	@Path("/melhorCaminho")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response obterMelhorCaminho(@QueryParam("origem") String origem, @QueryParam("destino") String destino, @QueryParam("autonomia") Double autonomia, @QueryParam("valorLitro") Double valorLitro) throws IOException {
		validarParametros(origem, destino, autonomia, valorLitro);
		
		LogisticaService service = new LogisticaService();
		RotaCompleta rotaCompleta = service.obterMelhorRota(origem, destino, autonomia, valorLitro);

		return Response.status(200).entity(rotaCompleta).build();
	}
	
	@POST
	@Path("/mapa")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response adicionarMapa(Mapa mapa) {
		LogisticaService svc = new LogisticaService();
		svc.adicionar(mapa);

		return Response.status(Response.Status.CREATED).build();
	}

	private void validarParametros(String origem, String destino, Double autonomia, Double valorLitro) {
		if (origem == null) {
			throwException(404, "Preencher a origem!");
		}
		
		if (destino == null) {
			throwException(404, "Preencher o destino!");
		}

		if (autonomia == null) {
			throwException(404, "Preencher a autonomia!");
		}

		if (valorLitro == null) {
			throwException(404, "Preencher o valor do litro!");
		}
	}
	
	private void throwException(int statusCode, String message) {
		java.util.Map<String, Object> result = new HashMap<String, Object>();
		result.put("code", statusCode);
		result.put("message", message);
				
		Gson gson = new Gson();
	    String json = gson.toJson(result);
	    
		Response response = Response.status(statusCode)
				.entity(json)
				.build();
		
		throw new WebApplicationException(response);
	}
	
}

package br.com.walmart.logistica.controller;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import br.com.walmart.logistica.model.Mapa;
import br.com.walmart.logistica.model.Rota;
import org.junit.Test;

import com.google.gson.Gson;

public class LogisticaControllerTest {

	@Test
	public void testeObterMelhorCaminho() {

		given().
				param("origem", "A").
				param("destino", "C").
				param("autonomia", "10").
				param("valorLitro", "5.5").
				expect().
				statusCode(200).
				when().
				get("/api-logistica/rota/melhorCaminho");

	}

	@Test
	public void testeCriarMapa() {
		List<Rota> rotas = new ArrayList<Rota>();
		Gson gson = new Gson();

		rotas.add(new Rota("A", "B", 15));
		rotas.add(new Rota("B", "C", 25));
		rotas.add(new Rota("C", "D", 30));
		rotas.add(new Rota("A", "D", 44));
		
		Mapa mapa = new Mapa("RJ", rotas);
		
	    String json = gson.toJson(mapa);

		given().contentType("application/json").body(json).expect().statusCode(201).when().post("/api-logistica/rota/mapa/");

	}

}
== Otimizando o tempo das entregas na logística

A empresa XYZ está desenvolvendo um novo sistema de logistica e sua ajuda é muito importante neste momento. Sua tarefa será desenvolver o novo sistema de entregas visando sempre o menor custo. Para popular sua base de dados o sistema precisa expor um Webservices que aceite o formato de malha logística (exemplo abaixo), nesta mesma requisição o requisitante deverá informar um nome para este mapa. É importante que os mapas sejam persistidos para evitar que a cada novo deploy todas as informações desapareçam. O formato de malha logística é bastante simples, cada linha mostra uma rota: ponto de origem, ponto de destino e distância entre os pontos em quilômetros.
A B 10
B D 15
A C 20
C D 30
B E 50
D E 30
Com os mapas carregados o requisitante irá procurar o menor valor de entrega e seu caminho, para isso ele passará o mapa, nome do ponto de origem, nome do ponto de destino, autonomia do caminhão (km/l) e o valor do litro do combustivel, agora sua tarefa é criar este Webservices. Um exemplo de entrada seria, mapa SP, origem A, destino D, autonomia 10, valor do litro 2,50; a resposta seria a rota A B D com custo de 6,25.
Voce está livre para definir a melhor arquitetura e tecnologias para solucionar este desafio, mas não se esqueça de contar sua motivação no arquivo README que deve acompanhar sua solução, junto com os detalhes de como executar seu programa. Documentação e testes serão avaliados também =) Lembre-se de que iremos executar seu código com malhas beeemm mais complexas, por isso é importante pensar em requisitos não funcionais também!!
Também gostariamos de acompanhar o desenvolvimento da sua aplicação através do código fonte. Por isso, solicitamos a criação de um repositório que seja compartilhado com a gente. Para o desenvolvimento desse sistema, nós solicitamos que você utilize a sua (ou as suas) linguagem de programação principal. Pode ser Java, JavaScript ou Ruby.

== Motivação

Participar da área de tecnologia de um líder na área de comércio eletrônico já é uma motivação bem considerável.
Pelo que conversei com alguns que trabalham na empresa, vocês estão sempre se reciclando, analisando novas tecnologias que podem agregar mais valor ao negócio e isso é algo que motiva grande parte dos profissionais de nossa área.
Fiz um curso de segurança em desenvolvimento web esses dias e notei que o instrutor que falava com propriedade era o responsável pela parte de segurança de vocês..
Isso mostra que vocês já estão bem mais preparados que as demais empresas em relação ao assunto pois normalmente as empresas não dão tanta importância para a segurança em sistemas.
Além destas motivações, estou em busca de qualidade de vida e moro a 3km de vocês.

== Projeto da entrevista

O projeto foi desenvolvido na linguagem Java 8 no IDE IntelliJ em um linux Slackware 64 bits.

O banco de dados utilizado foi o Neo4j, ele é um banco de dados NoSql de alta performance baseado em grafos preparado para resolver problemas como o proposto neste projeto.
Sendo um banco de dados baseado em grafos ele já possui integrado algoritmos para resolução de problemas relacionados a encontrar o menor caminho entre dois pontos, como o algoritmo de Dijkstra.

== Requisitos

Java 8
Neo4j Server 2.0.3
Jetty 9.2

== Configurando o sistema

=== WebServer Jetty

Utilizei o webserver Jetty, porém qualquer webserver pode ser utilizado para este projeto.

=== Neo4j

1 - Faça o [download](http://www.neo4j.org/download) do Neo4j Server 2.3.2 Community

2 - Extraia o conteúdo do arquivo

$ tar xvzf neo4j-community-2.3.2-unix.tar.gz

3 - Inicie o serviço

$ ./bin/neo4j start

== Utilização
=== Criar cada mapa de rotas

Abaixo segue um exemplo de requisição rest enviando via json o mapa:

POST: http://127.0.0.1:8080/api-logistica/rota/mapa/
Content-Type: application/json
Accept: application/json

Exemplo de dado a ser enviado:
	{
		"nome": "SP",
		"rotas": [
			{
				"origem": "A",
				"destino": "B",
				"distancia": 10
			},
			{
				"origem": "B",
				"destino": "D",
				"distancia": 15
			},
			{
				"origem": "A",
				"destino": "C",
				"distancia": 20
			},
			{
				"origem": "C",
				"destino": "D",
				"distancia": 30
			},
			{
				"origem": "B",
				"destino": "E",
				"distancia": 50
			},
			{
				"origem": "D",
				"destino": "E",
				"distancia": 30
			}
		]
	}

Exemplo de response:

201 CREATED

== Encontrar o melhor caminho

Abaixo segue um exemplo de requisição rest enviando via json a solicitação do melhor caminho para uma rota:

GET http://localhost:8080/api-logistica/rota/melhorCaminho?origem=A&destino=D&autonomia=10&valorLitro=2.5
Content-Type: application/json
Accept: application/json

Exemplo de response:

200 OK

	{
		"distancia": 25,
		"custo": 6.25,
		"caminho": [
			"A",
			"B",
			"D"
		]
	}

== Teste unitário JUnit

	Para o projeto foram criados dois testes unitários testeCriarMapa e testeObterMelhorCaminho.

== O que poderia ser melhorarado no projeto caso houvesse mais tempo?

-- Caso a informação seja sigilosa utilizar HTTPS para não expor em texto aberto os dados
-- Utilizar authenticação com um certificado digital para acessar o serviço (PEM) impedindo possível acesso não autorizado
-- Analisar a segurança do serviço REST utilizando o software de análise Arachni
-- Aumentar o número de testes unitários criando um code coverage de 85%
-- Automatizar o deploy com Jenkins
-- Criar um frontend amigável com AngularJS ou ReactJS
-- Mudar implementação de exposição REST da App para Spring Boot Rest colocando injeção de dependência nos serviços
-- Analisar viabilidade de colocar uma camada de persistência para o acesso ao neo4j
-- Gerar documentação automática e especificação de requisito para auxiliar em posterior manutenção
-- Realizar testes de stress com JMeter
-- Implementar log4j com severidades apropriadas